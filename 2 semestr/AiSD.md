# Algorytmy i struktury danych

## Algorytmy sortowania

### Z porównaniem (krótki opis)
- Bubble sort - `1/2 n ** 2`
- Insert sort - `1/4 n ** 2`
- Quick sort - `1.4 n * log(2, n)`
- Bin sort - `n * log(2, n)`
- Insert sort with bisection - `n * log(2, n)`

### Funkcje mieszające (hash functions)

#### Definicja

Załóżmy, że klucze (liczby całkowite) pochodzą ze zbioru `S`,
a te klucze można przeliczyć na liczby ze zbioru `[0, m-1]`

`|S| <= m`

Taką funkcję nazywamy funkcją mieszającą.

#### Własności funkcji mieszającej
- łatwo obliczalna
- każdy wynik `[0, m-1]` powinien być jednakowo prawdopodobny

#### Usuwanie kolizji
#. **Metoda łańcuchowa**:
Dla każdego wskaźnika (indeksu) tworzymy możliwość konstruowania
listy elementów wchodzących w kolizję.

#### Terminy ogólne

- **Funkcja mieszająca** (haszująca, skrótu) - funkcja,
która ma sygnaturę `K -> [0, m-1]`
- **Hash** - tzw. _skrót nieodracalny_
- **Kolizja** - sytuacja, gdy dwa różne kluczy funkcja
ma taki sam wynik.

#### Przykłady
- `h1(k) = k % m` m - liczba pierwsza
- `h1(k) = *M bitów wycinamy z liczby (c * k) % b*`
    - `m = 2 ** M`
    - `b = 2 ** B`
    - `B` - długość słowa
    maszyny
    - postać liczby `c` nie powinna się kończyć na x21
    z cyfrą parzystą x

### Adresowanie liniowe

Założenie: `n <= m`
