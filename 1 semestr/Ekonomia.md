# Podstawy ekonomii

## Organizacyjne

### Nauczycielka
- dr Magdalena Czerwińska
- Katedra Ekonomii i Zarządzania Godspodarką
- Wydział Zarządzania
- m.czerwinska@pollub.pl

### Konsultacje
- 10-12 środa
- pokój 243

### Zaliczenia
- 27 stycznia 2017

### Literatura
- _Begg, Fiszer, Dornbusch_: Mikroekonomia, Makroekonomia, zbiór zadań
- _Kamerschen, McKenzie, Nardinelli_: Ekonomia
- _Krugman, Wells_: Mikroekonomia, Makroekonomia
- _Samuelson, Nordhaus_: Ekonomia
- _Milewski_: Podstawy ekonomii
- _Milewski_: Elementarne Zagadnienia ekonomii
- _Czarny, Czarny, Bartkowiak, Rapacki_: Podstawy ekonomii
- _Duda, Mamcarz, Pakuła_: Ekonomia
- _red. Żukowski_: Zarys ekonomii

## Ekonomia jako nauka
- badanie działań człowieka dotyczących **produkcji** oraz wymiany między
ludźmi
- analizuje **zmiany** w całości gospodarki
- nauka o **dokonywaniu wyborów**
- nauka o **sposobie organizacji działań** w sferze **konsumpcji i produkcji**
- nauka o **pieniądzu**, stopie procentowej, kapitale i bogactwie
- nauka o procesach gospodarczych, wykrywająca i opisująca prawidłowości nimi
rządzące (prawa ekonomiczne)
- nauka gromadząca i porządkująca **prawdziwą wiedzę** o gospodarowaniu
- nauka badająca, w jaki sposób społeczeństwo gospodarujące decyduje o tym, co,
jak i dla kogo wytwarzać
- nauka o tym, jak jednostki i społeczeństwo decydują o wykorzystaniu rzadkich
zasobów - które mogą mieć także inne alternatywne zastosowa...

## Podstawowe problemy ekonomiczne
- **co** produkować i **ile**?
- **jak** się powinno produkować
    - technika wytwórcza - sposób produkowania
- **dla kogo** wytwarzać dobra

**Rzadkość** - fakt, że nie możemy mieć wszystkiego, co chcemy
przez cały czas; brak nieograniczonej dostępności dóbr

## Sposoby stosowania ekonomii
- opis, wyjaśnienie i prognozowanie zachowań w sferze produkcji, inflacji,
dochodów itd.
- poprawa efektów gospodarczych

## Pozytywna i normatywna

### Ekonomia pozytywna (opisowa)
- zajmuje się naukowym objaśnieniem zasad funkjonowania gospodarki
- zajmuje się opisem faktów, okoliczności i wzajemnych zależności w gospodarce
- stawia pytania, na które można odpowiedzieć odwołując się do faktów
- gałąź badań ekonomicznych, która zajmuje się światem takim, jakim jest

### Ekonomia normatywna
- dostarcza **zaleceń i rekomendacji** opartych na subiektywnych sądach
wartościujących
- stawia pytania, zawierające **oceny** wartościujące lub moralne - można
je rozażać, ale da się na nie odpowiedzieć na podstawie nauki czy
odwołania się do faktów
- gałąź badań ekonomicznych, która zajmuje się światem takim,
jakim być powinien

## Mikroekonomia i makroekonomia

### Mikroekonomia
- zajmuje się szczegółowym badaniem indywidualnych **decyzji** dotyczących
pojedynczych **towarów**
- bada poszczególne **elementy tworzące gospodarkę**
(gospodarstwa domowe, przedsiębiorstwa, sektory i gałęzie gospodarki,
rynki określonych produktów i usług itd.)
- patrzy na gospodarkę narodową przez pryzmat **przedsiębiorców i
konsumentów** z punktu widzenia **maksymalizacji ich korzyści**

### Makroekonomia
- kładzie nacisk na **wzajemne związki** zachodzące w gospodarce jako całości
- celowo uraszcza analizę poszczególnych **elementów** badanej całości
w trosce o przejrzystość obrazu działania całej gospodarki
- bada wielkości agregatowe
- bada zależności pomiędzy takimi agregatami
    - dochód narodowy
    - wydatki na konsumpcję
    - wydatki na inwestycje
    - oszczędności
    - dochody i wydatki budżety państa bilans handlowy i płatniczy kraju
    - rozmiary zatrudnienia i bezrobocia w kraju itp.

### Ekonomia a inne nauki
- prawo
- matematyka
- historia gospodarcza
- geografia ekonomiczna
- prakseologia
- nauki społeczne
- statystyka ekonomiczna
- logika
- ekonomika

### Prawa ekonomiczne

#### Rodzaje praw ekonomicznych
#. Przyczynowe
#. współistnienia
#. funkcjonalne

#### Cechy praw ekonomicznych
#. obiektywne
#. stochastyczne
