# Podstawy programowania

Wydział Mechaniczny, Aula 1

## Profesor
- dr hab. inż. Jerzy Montusiewicz, prof. PL
- pokój 110
- j.montusiewicz@pollub.pl
- Instytut Informatyki

## Godziny
- 30 godzin wykładów
- 30 godzin lekcji

## Literatura
- _Montusiewicz, Miłosz, Jarosińska-Caban M_: Podstawy programowania w
język C. Ćwiczenia laboratoryjne. Lublin 2015.

## Typy
- proceduralne
- funkcyjne
- zdarzeniowe
- objektowe

## Słownictwo
- pętle - циклы
- wybor - ветвление
- wywołanie - вызов
- klamry - фигурные скобки

## Etapy
- Określenie celów
- Projektowanie, algorytm
- Pisanie
- Kompilacja
- Uruhcomienie
- Testowanie i usuwanie błędów
- Pielęgnowanie i modyfikacja

## Algorytm
- skończony
- uporządkowany

## Schemat Nassi-Schneidermanna
nevermind
