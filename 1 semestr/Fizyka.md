# Fizyka

## Windoz guys
- [windows guys homepage](pollub.net)
- [Piotr Siemieniuk](mailto://piotr.siemieniuk@studentpartner.com)
- spotkanie 17 października 18:15

## Nauczyciel
- [Tomasz Pikula](mailto://t.pikula@pollub.pl)
- [Ćwiczenia](zeft.pollub.pl)
- pokój C517
- konsultacja: Poniedziałek 13-14
- Semestry
    - I
        - 30 g
        - zaliczenie
    - II
        - 15 g
        - egzamin
        - laboratoria

## Literatura
- R. Resnick, D. Holliday: Fizyka
- Cz. Bobrowski: Fizyka. Krótki kurs.

## Wstęp matematyczny

### Wektory

#### (vector properties)
- kierunek
- zwrot
- wartość

#### (scalar property)
- wartość

#### Notacja
- `C = [3, 2] = 3 i + 2 j`
- _i_ i _j_ - wektory jednostkowe (wersory)

#### Wartość wektora
`|A| = sqrt(a**2 + b**2)`

#### Mnożenia wektorów
- skalarne (`sum([x * y for x, y in zip(a, b)])` albo `|A| * |B| * cos(alpha)`)
- wektorowe (wyznacznikiem) (`[[i, j, k], *a, *b]` oraz `|A x B| = |A| * |B| * sin(alpha)`)

## Kinematyka

- opis ruchu
- położenie, droga
- prędkość
- przyspieszenie

### Ruch

#### Podstawowe pojęcia
*Ruch* - zmiana położenia względem pewnego układu odniesienia.
*Względność ruchu*
*Punkt materialny* - teoretyczny twór, któr posiada masę ale nie posiada rozmiarów w przestrzeni.

#### Położenie
- wektor położenia - used to tell the position of an object instead of basic coordinates
- wektor przemieszczenia - used to tell the change of position of an object
- `r(t) = 2 t i + t**2 j + 5 k` - funkcja, z jakiej korzystujemy, żeby opisać ruch

#### Prędkość
Prędkość - pochodna wektora położenia po czasie. (prędkość chwilowa)
- `V = dr / dt`
- `V(t) = dr(t) / dt = dx(t)/dt * i + dy(t)/dt * j + dz(t) / dt * k`
- `|V(t)| = sqrt(Vx**2 + Vy**2 + Vz**2)`

##### Przykład
- `r1(t) = t**2 i + 2 t j`
- `r2(t) = t**2 i + 3 j`


#. wektory prędkości
#. narysować tory ruchu ciąstek
#. czy cząstki te spotkają się? (przyrównać wektory otrzymując czas spotkania)
#. obliczyć wartość prędkośći ciała pierwszego


#### Przyspieszenie
- pośrednione `a = ðV/ðt`
- chwilowe `a = dV/dt`

#### Klasyfikacja ruchów

##### Ruch (tor):
- prostoliniowy
- krzywoliniowy

##### Ruch (przyspieszenie):
- jednostajny
- niejednostajny
    - jednostajnie zmienny
        - przyspieszony
        - opóźniony
    - niejednostajnie zmienny

#### Ruch jednostajny
`r = V * t + r0`
`S = V * t + s0`

#### Ruch jednostajnie zmienny
- `a = const`
- `V = całka(a, dt) = a * całka(1, dt) = a * t + V0`
- `r = całka(V, dt) = V0 * t + a * t**2/2 + r0`

#### Rzut ukośny
- V0 = 200 m/s
- kąt = 30°
- napisz równanie ruchu pocisku

polecenia:
- oblicz ti
- oblicz z
- h max
- kąt = ? aby z = max

- `r(t) = V0x * t * i + (V0y * T - g * t**2 / 2) * j`
- `sin(a) = V0y/V0`
- `V0y = V0 sin(a)`

- `cos(a) = V0x/V0`
- `V0x = V0 cos(a)``

- `r(t) = (V0 * cos(a) * t) * i + (V0 * sin(a) * t - g * t**2 / 2) * j`

- `t = 2 * V0 sin(a) / g`

## Dynamika punktu materialnego


