# Teoretyczne podstawy informatyki

## Nauczyciel
- Stanisław Grzegórski
- profesor

## Zaliczenie
- Egzaminy

## Informacja

### Problemy informatyczne
Działania na informacji:
#. zbieranie
#. przechowywanie
#. przesyłanie
#. przetwarzanie
#. interpretowanie

### Podstawowe pojęcia
*Zadanie* - zestaw danych, pytanie lub zestaw pytań na które powinnismy
uzyskać odpowiedź analizując dane.


*Algorytm* - opis stuktury danych i zestaw czynności (operacji na danych)


#### Rodzaje instrukcji
#. wejście
#. wyjścia
#. podstawienia(przypisania)
#. warunkowa
#. cyklu (pętli)

## Automaty skończone

_some skipped lectures_

### Twierdzenie (o automatach niepustych)
- Zbiór słów akceptowanych przez automat skończony (o _n_ stanach) jest niepusty
wtedy i tylko wtedy, gdy akceptuje słowo o długości mniejszej od _n_.
- Zbiór słów akceptowanych przez automat skończony o _n_ stanach jest nieskończony
jeśli akceptuje słowo o długości _L_, gdzie n <= _L_ < 2n

### Twierdzenie (o równoważności)
- Istnieje algorytm rozstrzyganie czy dwa automaty skończone są równoważne tzn.
akceptują ten sam język (zbiór słów).

`L3 = (L1 * -L2) + (-L1 * L2)`

### Twierdzenie
- Dla każdego zbioru regularnego _L_ istnieje wyznaczony jednoznacznie z dokładnością
do izomorfizmu (przemianowanie stanów) automat skończony o minimalnej liczbie stanów
akceptujący _L_.

### Własonści języków rekurencyjnych i rekurencyjnie przeliczalnych

JR <= JRP

#. Dopełnienie JR jest JR.
#. Suma teoriomnogościowa 2 JR jest JR.
#. Suma teoriomnogościowa 2 JRP jest JRP.
#. Jeśli zarówno język L i \~L są JRP to L i \~L są JR.

Problem, któremu odpowiada JR nazywamy **rozstrzygalnym**.
W przeciwnym razie jest to problem **nierozstrzygalny**.

## Teoria złożoności obliczeniowej

- złożoność czasowa
- złożoność pamięciowa

### Definicje
**Złożoność pamięciowa.** Niech M oznacza wsadową MT.
Jeżeli przy każdym słowie wejciowym długości n, MT
czyta co najwyżej P(n) komówek na dowolnej taśmie
pamięci, to mówimy, że MT jest maszyną z ograniczeniem
miejsca P(n) lub inaczej o złożoności pamięciowej P(n).

**Złożoność czasowa.** Niech M oznacza MT z k taśmami
dwustronnie nieskończonymi. Wejście znajduje się na
1 z tych taśm. Jeśli dla każdego słowa wejściowego o
długośi n maszyna M wykonuje C(n) ruchów, to mówimy,
że M jest MT z ograniczeniem czasowym C(n) lub o
złożoności czasowej C(n).

#. `P(n) >= n`
#. `C(n) >= n + 1`
#. `C(n) >= log2(n)`

#### Oznaczenia
# DPAM(P(n)) - rodzina języków o złożoności pamięciowej P(n)
